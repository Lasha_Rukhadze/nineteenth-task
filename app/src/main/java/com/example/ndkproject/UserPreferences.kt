package com.example.ndkproject

import android.content.Context
import android.content.SharedPreferences

object UserPreference {

    private const val USER_NAME : String = "USER_NAME"

    private val sharedPreferences : SharedPreferences by lazy {
        App.context!!.getSharedPreferences("local", Context.MODE_PRIVATE)
    }

    fun saveUserName(userName : String) {
        sharedPreferences.edit().putString(USER_NAME, userName).apply()
    }

    fun getUserName(): String? {
        return sharedPreferences.getString(USER_NAME, " ")
    }

    fun clearLocal(){
        sharedPreferences.edit().clear().apply()
    }

    fun removeUsingKey(key : String) {
        if (sharedPreferences.contains(key))  sharedPreferences.edit().remove(key).apply()
    }
}