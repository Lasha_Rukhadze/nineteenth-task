package com.example.ndkproject

import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserViewModel(private val user: User) : ViewModel() {

    private var myResponse1 : MutableLiveData<Register> = MutableLiveData()
    private var myResponse2 : MutableLiveData<SignIn> = MutableLiveData()
    //private val user = User(email, password, returnSecureToken)

    fun getMyResponse1() : LiveData<Register> {
        return myResponse1
    }
    fun getMyResponse2() : LiveData<SignIn> {
        return myResponse2
    }

    fun init1(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getRegisterResponse()
            }
        }
    }

    fun init2(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getSignInResponse()
            }
        }
    }

    private suspend fun getRegisterResponse() {
        val response = RetrofitService.retrofitService.register(user)
        if (response.isSuccessful)
        {
            val result = response.body()
            myResponse1.postValue(result)
        }
        else {
            response.code()
        }
    }

    private suspend fun getSignInResponse(){
        val response = RetrofitService.retrofitService.signIn(user)
        if (response.isSuccessful){
            val result = response.body()
            myResponse2.postValue(result)
        }
        else {
            response.code()
        }
    }

    class Factory(private val user : User) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return UserViewModel(user) as T
        }
    }
}