package com.example.ndkproject

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface RetrofitRepository {

    @POST("/v1/accounts:signUp?")
    suspend fun register(
        @Body user: User,
        @Query("key")
        apiKey : String = App.apiKey
    ) : Response<Register>

    @POST("/v1/accounts:signInWithPassword?")
    suspend fun signIn(
        @Body user: User,
        @Query("key")
        apiKey: String = App.apiKey
    ) : Response<SignIn>
}