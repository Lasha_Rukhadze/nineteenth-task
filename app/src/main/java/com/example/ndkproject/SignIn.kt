package com.example.ndkproject

data class SignIn(val idToken: String?,
                  val email : String?,
                  val refreshToken : String?,
                  val registered : Boolean?
){

}
