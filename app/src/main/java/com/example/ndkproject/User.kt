package com.example.ndkproject

data class User(val email : String,
                val password : String,
                val returnSecureToken : Boolean = true
){

}
