package com.example.ndkproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.ndkproject.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    lateinit var binding : FragmentFirstBinding
    lateinit var user: User
    private val userViewModel : UserViewModel by viewModels {
        UserViewModel.Factory(user)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentFirstBinding.inflate(inflater, container, false)

        binding.createAccountButton?.setOnClickListener {
            if (check()){
                userViewModel.init1()
                Navigation.findNavController(binding.root).navigate(R.id.action_firstFragment_to_secondFragment)
            }
        }
        binding.signInButton1?.setOnClickListener {
            if (check()){
                userViewModel.init2()
                observes()
            }
        }

        return binding.root
    }

    private fun check() : Boolean {
        if (binding.email?.text.toString().isNotBlank() && binding.password?.text.toString().isNotBlank()){
            user = User(binding.email?.text.toString(), binding.password?.text.toString())
            return true
        }
        else {
            Toast.makeText(this.context, "All fields MUST be filled", Toast.LENGTH_LONG).show()
        }
        return false
    }

    private fun observes(){
        userViewModel.getMyResponse2().observe(viewLifecycleOwner, {
            if (it.registered==true && binding.email?.text.toString() == it.email){
                Navigation.findNavController(binding.root).navigate(R.id.action_firstFragment_to_secondFragment)
            }
        })
    }

}