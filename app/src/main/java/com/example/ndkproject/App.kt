package com.example.ndkproject

import android.app.Application
import android.content.Context

class App : Application() {

    private external fun getApiKey(): String

    override fun onCreate() {
        super.onCreate()
        context = this
        apiKey = getApiKey()
    }

    companion object {
        var context : Context? = null
        lateinit var apiKey : String

        init {
            System.loadLibrary("native-lib")
        }
    }
}